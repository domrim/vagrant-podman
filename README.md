# Vagrant VM for podman

## Prepare
Install parallels, podman, vagrant & ansible
```bash
brew install parallels vagrant ansible podman
vagrant plugin install vagrant-parallels
```

## Setup
```bash
vagrant up
podman system connection add --identity $(pwd)/.vagrant/machines/default/parallels/private_key -d vagrant ssh://vagrant@localhost:2222
```

## References & Links
* https://vikaspogu.dev/posts/podman-macos/
* http://www.justinleegrant.com/2021/01/25/setting-up-podman.html
* https://www.redhat.com/sysadmin/replace-docker-podman-macos
